<?php
class TMC_Clock {
	/**
 	 * Called when the extension is first loaded
	 */
	public static function onRegistration()
	{
		global $wgExtensionFunctions;
		$wgExtensionFunctions[] = __CLASS__ . '::setup';
	}

	public static function setup()
	{
		global $wgOut, $wgResourceModules, $wgExtensionAssetsPath, $IP, $wgAutoloadClasses;

		$parser = \MediaWiki\MediaWikiServices::getInstance()->getParser();

		// Register the parser-function
		$parser->setFunctionHook( 'tmc_analogclock', __CLASS__ . '::render' );

		// This gets the remote path even if it's a symlink (MW1.25+)
		$path = $wgExtensionAssetsPath . str_replace( "$IP/extensions", '', dirname( $wgAutoloadClasses[__CLASS__] ) );
		$wgResourceModules['ext.tmc_analogclock']['remoteExtPath'] = $path;
		$wgOut->addModules( 'ext.tmc_analogclock' );
	}

	/**
	 * Expands the "w_radar" magic word to a table of all the individual month tables
	 */
	public static function render( $parser )
	{
		$time = '<div id="live_clock" class="clock">';
		$time .= '<div class="outer_face">';
		$time .= '<div class="marker oneseven"></div>';
		$time .= '<div class="marker twoeight"></div>';
		$time .= '<div class="marker fourten"></div>';
		$time .= '<div class="marker fiveeleven"></div>';
		$time .= '<div class="inner_face">';
		$time .= '<div id=hour_hand class="hand hour"></div>';
		$time .= '<div id=minute_hand class="hand minute"></div>';
		$time .= '<div id=second_hand class="hand second"></div>';
		$time .= '</div></div>';
		$time .= '<div id="break_text" class="clock_caption"></div>';
		$time .= '</div>';
		$time .= '<script language="javascript" src="scripts/jquery.min.js"></script>';
		$time .= '<script language="javascript" src="scripts/tmc_clock.js"></script>';
		$time .= '<script type="text/javascript">';
		$time .= 'requestAnimationFrame(updateclock);';
		$time .= '</script>';

		return array( $time, 'isHTML' => true, 'noparse' => true );
	}
}
