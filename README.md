# MediaWiki Analog Clock


This is for displaying an analog clock inside a MediaWiki page and as such is a MediaWiki extension.

It was modified from exisiting javascript code to be made into the extension, and as such will load javascript to the MediaWiki.

Place the files under TMC_Clock inside your extensions folder and enable it in your LocalSettings.php file with the line: wfLoadExtension('TMC_Calendar');
Then use the keyword: {{ #tmc_analogclock: title=Analog Clock }} (or similar) to activate it on your MediaWiki


The necessary javascript files are located in the scripts directory and should be placed in a location off of the root (public) directory of the site. There are some 'hard-coded' messages as well as a 'hard-coded' alarm in the tmc_clock.js. If you already have a copy of jquery you can use it, it's only provided here so you don't need to look for it.
